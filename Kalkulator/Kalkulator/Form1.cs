﻿//Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost
//znanstvenog kalkulatora, odnosno implementirati osnovne(+,-,*,/) i barem 5
//naprednih(sin, cos, log, sqrt...) operacija.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalkulator
{
    public partial class Kalkulator : Form
    {
        public Kalkulator()
        {
            InitializeComponent();
        }

        private void button_plus_Click(object sender, EventArgs e)
        {
            double a, b;
            if((!double.TryParse(textBox_a.Text, out a)) || (!double.TryParse(textBox_b.Text, out b)))
            {
                MessageBox.Show("Neispravan unos!", "Tehnicka greska");
            }
            else label_output.Text = (a + b).ToString();
        }

        private void button_minus_Click(object sender, EventArgs e)
        {
            double a, b;
            if ((!double.TryParse(textBox_a.Text, out a)) || (!double.TryParse(textBox_b.Text, out b)))
            {
                MessageBox.Show("Neispravan unos!", "Tehnicka greska");
            }
            else label_output.Text = (a - b).ToString();
        }

        private void button_multiply_Click(object sender, EventArgs e)
        {
            double a, b;
            if ((!double.TryParse(textBox_a.Text, out a)) || (!double.TryParse(textBox_b.Text, out b)))
            {
                MessageBox.Show("Neispravan unos!", "Tehnicka greska");
            }
            else label_output.Text = (a * b).ToString();
        }

        private void button_divide_Click(object sender, EventArgs e)
        {
            double a, b;
            if ((!double.TryParse(textBox_a.Text, out a)) || (!double.TryParse(textBox_b.Text, out b)))
            {
                MessageBox.Show("Neispravan unos!", "Tehnicka greska");
            }
            else if(b == 0)
            {
                MessageBox.Show("Uneseni nazivnik jednak nuli!", "Logicka greska");
            }
            else label_output.Text = (a / b).ToString();
        }

        private void button_power2_Click(object sender, EventArgs e)
        {
            double a;
            if (!double.TryParse(textBox_a.Text, out a))
            {
                MessageBox.Show("Neispravan unos!", "Tehnicka greska");
            }
            else label_output.Text = (a * a).ToString();
        }

        private void button_power_Click(object sender, EventArgs e)
        {
            double a, b;
            if ((!double.TryParse(textBox_a.Text, out a)) || (!double.TryParse(textBox_b.Text, out b)))
            {
                MessageBox.Show("Neispravan unos!", "Tehnicka greska");
            }
            else label_output.Text = (Math.Pow(a,b)).ToString();
        }

        private void button_sqrt_Click(object sender, EventArgs e)
        {
            double a;
            if (!double.TryParse(textBox_a.Text, out a))
            {
                MessageBox.Show("Neispravan unos!", "Tehnicka greska");
            }
            else if (a < 0)
            {
                MessageBox.Show("Uneseni broj je manji od nule!", "Logicka greska");
            }
            else label_output.Text = (Math.Sqrt(a)).ToString();
        }

        private void button_e_Click(object sender, EventArgs e)
        {
            double a;
            if (!double.TryParse(textBox_a.Text, out a))
            {
                MessageBox.Show("Neispravan unos!", "Tehnicka greska");
            }
            else label_output.Text = (Math.Pow(2.72,a)).ToString();
        }

        private void button_log_Click(object sender, EventArgs e)
        {
            double a;
            if (!double.TryParse(textBox_a.Text, out a))
            {
                MessageBox.Show("Neispravan unos!", "Tehnicka greska");
            }
            else if(a <= 0)
            {
                MessageBox.Show("Uneseni broj je manji ili jednak 0!", "Logicka greska");
            }
            else label_output.Text = (Math.Log(a)).ToString();
        }
    }
}
