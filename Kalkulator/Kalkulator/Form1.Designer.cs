﻿namespace Kalkulator
{
    partial class Kalkulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_a = new System.Windows.Forms.TextBox();
            this.textBox_b = new System.Windows.Forms.TextBox();
            this.label_a = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button_plus = new System.Windows.Forms.Button();
            this.button_minus = new System.Windows.Forms.Button();
            this.button_multiply = new System.Windows.Forms.Button();
            this.button_divide = new System.Windows.Forms.Button();
            this.button_power2 = new System.Windows.Forms.Button();
            this.button_power = new System.Windows.Forms.Button();
            this.button_sqrt = new System.Windows.Forms.Button();
            this.button_e = new System.Windows.Forms.Button();
            this.button_log = new System.Windows.Forms.Button();
            this.label_rezultat = new System.Windows.Forms.Label();
            this.label_output = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox_a
            // 
            this.textBox_a.Location = new System.Drawing.Point(91, 12);
            this.textBox_a.Name = "textBox_a";
            this.textBox_a.Size = new System.Drawing.Size(100, 20);
            this.textBox_a.TabIndex = 0;
            // 
            // textBox_b
            // 
            this.textBox_b.Location = new System.Drawing.Point(91, 38);
            this.textBox_b.Name = "textBox_b";
            this.textBox_b.Size = new System.Drawing.Size(100, 20);
            this.textBox_b.TabIndex = 1;
            // 
            // label_a
            // 
            this.label_a.AutoSize = true;
            this.label_a.Location = new System.Drawing.Point(69, 15);
            this.label_a.Name = "label_a";
            this.label_a.Size = new System.Drawing.Size(16, 13);
            this.label_a.TabIndex = 2;
            this.label_a.Text = "a:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "b:";
            // 
            // button_plus
            // 
            this.button_plus.Location = new System.Drawing.Point(10, 82);
            this.button_plus.Name = "button_plus";
            this.button_plus.Size = new System.Drawing.Size(75, 23);
            this.button_plus.TabIndex = 4;
            this.button_plus.Text = "a + b";
            this.button_plus.UseVisualStyleBackColor = true;
            this.button_plus.Click += new System.EventHandler(this.button_plus_Click);
            // 
            // button_minus
            // 
            this.button_minus.Location = new System.Drawing.Point(91, 82);
            this.button_minus.Name = "button_minus";
            this.button_minus.Size = new System.Drawing.Size(75, 23);
            this.button_minus.TabIndex = 5;
            this.button_minus.Text = "a - b";
            this.button_minus.UseVisualStyleBackColor = true;
            this.button_minus.Click += new System.EventHandler(this.button_minus_Click);
            // 
            // button_multiply
            // 
            this.button_multiply.Location = new System.Drawing.Point(172, 82);
            this.button_multiply.Name = "button_multiply";
            this.button_multiply.Size = new System.Drawing.Size(75, 23);
            this.button_multiply.TabIndex = 6;
            this.button_multiply.Text = "a * b";
            this.button_multiply.UseVisualStyleBackColor = true;
            this.button_multiply.Click += new System.EventHandler(this.button_multiply_Click);
            // 
            // button_divide
            // 
            this.button_divide.Location = new System.Drawing.Point(10, 111);
            this.button_divide.Name = "button_divide";
            this.button_divide.Size = new System.Drawing.Size(75, 23);
            this.button_divide.TabIndex = 7;
            this.button_divide.Text = "a / b";
            this.button_divide.UseVisualStyleBackColor = true;
            this.button_divide.Click += new System.EventHandler(this.button_divide_Click);
            // 
            // button_power2
            // 
            this.button_power2.Location = new System.Drawing.Point(91, 111);
            this.button_power2.Name = "button_power2";
            this.button_power2.Size = new System.Drawing.Size(75, 23);
            this.button_power2.TabIndex = 8;
            this.button_power2.Text = "a ^ 2";
            this.button_power2.UseVisualStyleBackColor = true;
            this.button_power2.Click += new System.EventHandler(this.button_power2_Click);
            // 
            // button_power
            // 
            this.button_power.Location = new System.Drawing.Point(172, 111);
            this.button_power.Name = "button_power";
            this.button_power.Size = new System.Drawing.Size(75, 23);
            this.button_power.TabIndex = 9;
            this.button_power.Text = "a ^ b";
            this.button_power.UseVisualStyleBackColor = true;
            this.button_power.Click += new System.EventHandler(this.button_power_Click);
            // 
            // button_sqrt
            // 
            this.button_sqrt.Location = new System.Drawing.Point(10, 140);
            this.button_sqrt.Name = "button_sqrt";
            this.button_sqrt.Size = new System.Drawing.Size(75, 23);
            this.button_sqrt.TabIndex = 10;
            this.button_sqrt.Text = "√a";
            this.button_sqrt.UseVisualStyleBackColor = true;
            this.button_sqrt.Click += new System.EventHandler(this.button_sqrt_Click);
            // 
            // button_e
            // 
            this.button_e.Location = new System.Drawing.Point(91, 140);
            this.button_e.Name = "button_e";
            this.button_e.Size = new System.Drawing.Size(75, 23);
            this.button_e.TabIndex = 11;
            this.button_e.Text = "e ^ a";
            this.button_e.UseVisualStyleBackColor = true;
            this.button_e.Click += new System.EventHandler(this.button_e_Click);
            // 
            // button_log
            // 
            this.button_log.Location = new System.Drawing.Point(172, 140);
            this.button_log.Name = "button_log";
            this.button_log.Size = new System.Drawing.Size(75, 23);
            this.button_log.TabIndex = 12;
            this.button_log.Text = "log(a)";
            this.button_log.UseVisualStyleBackColor = true;
            this.button_log.Click += new System.EventHandler(this.button_log_Click);
            // 
            // label_rezultat
            // 
            this.label_rezultat.AutoSize = true;
            this.label_rezultat.Font = new System.Drawing.Font("Sitka Display", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_rezultat.Location = new System.Drawing.Point(12, 192);
            this.label_rezultat.Name = "label_rezultat";
            this.label_rezultat.Size = new System.Drawing.Size(85, 28);
            this.label_rezultat.TabIndex = 13;
            this.label_rezultat.Text = "Rezultat: ";
            // 
            // label_output
            // 
            this.label_output.AutoSize = true;
            this.label_output.Font = new System.Drawing.Font("Sitka Display", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_output.Location = new System.Drawing.Point(103, 192);
            this.label_output.Name = "label_output";
            this.label_output.Size = new System.Drawing.Size(0, 28);
            this.label_output.TabIndex = 14;
            // 
            // Kalkulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.label_output);
            this.Controls.Add(this.label_rezultat);
            this.Controls.Add(this.button_log);
            this.Controls.Add(this.button_e);
            this.Controls.Add(this.button_sqrt);
            this.Controls.Add(this.button_power);
            this.Controls.Add(this.button_power2);
            this.Controls.Add(this.button_divide);
            this.Controls.Add(this.button_multiply);
            this.Controls.Add(this.button_minus);
            this.Controls.Add(this.button_plus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label_a);
            this.Controls.Add(this.textBox_b);
            this.Controls.Add(this.textBox_a);
            this.Name = "Kalkulator";
            this.Text = "Kalkulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_a;
        private System.Windows.Forms.TextBox textBox_b;
        private System.Windows.Forms.Label label_a;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button_plus;
        private System.Windows.Forms.Button button_minus;
        private System.Windows.Forms.Button button_multiply;
        private System.Windows.Forms.Button button_divide;
        private System.Windows.Forms.Button button_power2;
        private System.Windows.Forms.Button button_power;
        private System.Windows.Forms.Button button_sqrt;
        private System.Windows.Forms.Button button_e;
        private System.Windows.Forms.Button button_log;
        private System.Windows.Forms.Label label_rezultat;
        private System.Windows.Forms.Label label_output;
    }
}

